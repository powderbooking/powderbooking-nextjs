'use client'
import { Button, ButtonProps } from '@/components/button/Button'

export default function ButtonExample({
  label,
  testid,
}: Omit<ButtonProps, 'onClick'>): JSX.Element {
  return (
    <Button
      label={label}
      testid={testid}
      onClick={() => console.log('clicked')}
    />
  )
}
