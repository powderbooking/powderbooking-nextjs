'use client'
import { Paragraph } from '@/components/paragraph/Paragraph'

export default function Error() {
  return <Paragraph text={'Something went wrong. Sorry :('} />
}
