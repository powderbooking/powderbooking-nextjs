import { Pagination } from '@/components/pagination/Pagination'
import { PaginationPillProps } from '@/components/pagination/PaginationPill'
import { OverviewDtoRankedBatchDto } from '@/lib'

export const OverviewPagination = ({
  rankingId,
  lastRank,
  firstRank,
}: Omit<OverviewDtoRankedBatchDto, 'batch'>) => {
  if (!firstRank && !lastRank) return null

  const pills: PaginationPillProps[] = []
  if (firstRank) {
    pills.push({
      href: `/?action=previous&rankingId=${rankingId}&rank=${firstRank}`,
      type: 'previous',
      testid: 'overview-pagination-previous',
    })
  }
  if (lastRank) {
    pills.push({
      href: `/?action=next&rankingId=${rankingId}&rank=${lastRank}`,
      type: 'next',
      testid: 'overview-pagination-next',
    })
  }

  return (
    <Pagination
      pills={pills}
      testid={'overview-pagination'}
      className="my-2 justify-end"
    />
  )
}
