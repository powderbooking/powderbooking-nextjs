import { http, HttpResponse } from 'msw'
import { BASE_PATH } from '@/lib'
import { mockedResortJson, mockedRankedBatchDtoJson } from '@/mocks/models'

export const handlers = [
  http.get(BASE_PATH + '/rank/forecast', () => {
    return HttpResponse.json(mockedRankedBatchDtoJson)
  }),

  http.get(BASE_PATH + '/resort/:id', ({ params }) => {
    const { id } = params
    return HttpResponse.json(mockedResortJson)
  }),
]
