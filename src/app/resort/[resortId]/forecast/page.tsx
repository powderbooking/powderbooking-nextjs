import { ForecastApi } from '@/lib'
import { ChartSnow } from '@/components/charts/ChartSnow'
import { Title } from '@/components/title/Title'
import { ChartTemp } from '@/components/charts/ChartTemp'
import { Paragraph } from '@/components/paragraph/Paragraph'
import { ResortPageProps } from '../types'

const dateFormatter = new Intl.DateTimeFormat('nl-NL')

export default async function Page({ params: { resortId } }: ResortPageProps) {
  const forecast = await new ForecastApi().forecastGetCurrent({ resortId })

  return (
    <>
      {forecast.forecastDays.length > 0 && (
        <Paragraph
          text={'Last updated on ' + dateFormatter.format(forecast.forecastDays[0]?.date)}
        />
      )}
      <Title
        text={'Snow and rain'}
        className={'pt-4'}
      />
      <ChartSnow
        labels={forecast.forecastDays.map(f => dateFormatter.format(f.date))}
        snow={forecast.forecastDays.map(f => f.snowTotalMm ?? 0)}
        rain={forecast.forecastDays.map(f => f.rainTotalMm ?? 0)}
      />
      <Title
        text={'Temperature'}
        className={'pt-4'}
      />
      <ChartTemp
        labels={forecast.forecastDays.map(f => dateFormatter.format(f.date))}
        tempMax={forecast.forecastDays.map(f => f.temperatureMaxC ?? 0)}
        tempMin={forecast.forecastDays.map(f => f.temperatureMinC ?? 0)}
      />
    </>
  )
}
