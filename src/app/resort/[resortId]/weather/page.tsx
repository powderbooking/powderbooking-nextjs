import { ForecastApi, WeatherApi } from '@/lib'
import { Paragraph } from '@/components/paragraph/Paragraph'
import { Title } from '@/components/title/Title'
import { ResortPageProps } from '../types'
import { List, RowData } from '@/components/list/List'

const dateFormatter = new Intl.DateTimeFormat('nl-NL', {
  dateStyle: 'medium',
  timeStyle: 'medium',
})

interface WeatherData extends RowData {
  label: string
  current: string | number
  prev1: string | number
  prev2: string | number
  prev3: string | number
}

const unknownSymbol = '0'
const headers = ['', 'latest report', '-1', '-3', '-7']
const selectors: (keyof WeatherData)[] = ['label', 'current', 'prev1', 'prev2', 'prev3']
const cn = [
  'flex-none w-40 text-left',
  'flex-1 text-left',
  'flex-none w-24 text-center',
  'flex-none w-24 text-center',
  'flex-none w-24 text-center',
]

export default async function Page({ params: { resortId } }: ResortPageProps) {
  const weather = await new WeatherApi().weatherGet({ resortId })
  const past = await new ForecastApi().forecastGetPast({ resortId })

  const tempData: WeatherData[] = [
    {
      id: 'max',
      label: 'max',
      current: '',
      prev1: past.find(p => p.timepoint === 0)?.temperatureMaxC ?? '',
      prev2: past.find(p => p.timepoint === 2)?.temperatureMaxC ?? '',
      prev3: past.find(p => p.timepoint === 6)?.temperatureMaxC ?? '',
    },
    {
      id: 'current',
      label: 'current',
      current: weather.temperatureC ?? '',
      prev1: '',
      prev2: '',
      prev3: '',
    },
    {
      id: 'min',
      label: 'min',
      current: '',
      prev1: past.find(p => p.timepoint === 0)?.temperatureMinC ?? '',
      prev2: past.find(p => p.timepoint === 2)?.temperatureMinC ?? '',
      prev3: past.find(p => p.timepoint === 6)?.temperatureMinC ?? '',
    },
  ]

  const precipitationData: WeatherData[] = [
    {
      id: 'snow',
      label: 'Snow',
      current: '',
      prev1: past.find(p => p.timepoint === 0)?.snowTotalMm ?? '',
      prev2: past.find(p => p.timepoint === 2)?.snowTotalMm ?? '',
      prev3: past.find(p => p.timepoint === 6)?.snowTotalMm ?? '',
    },
    {
      id: 'snow-3h',
      label: '(last 3 hours)',
      current: weather.snow3hMm ?? unknownSymbol,
      prev1: '',
      prev2: '',
      prev3: '',
    },
    {
      id: 'rain',
      label: 'Rain',
      current: '',
      prev1: past.find(p => p.timepoint === 0)?.rainTotalMm ?? '',
      prev2: past.find(p => p.timepoint === 2)?.rainTotalMm ?? '',
      prev3: past.find(p => p.timepoint === 6)?.rainTotalMm ?? '',
    },
    {
      id: 'rain-3h',
      label: '(last 3 hours)',
      current: weather.rain3hMm ?? unknownSymbol,
      prev1: '',
      prev2: '',
      prev3: '',
    },
  ]

  return (
    <>
      <Paragraph text={'Last updated on ' + dateFormatter.format(weather.date)} />
      <Paragraph
        text={
          'Current weather report and previous predictions are shown (from yesterday, 3 days ago and a week ago).' +
          ' Perhaps they are giving you an indication how reliable the predictions are.'
        }
        className={'pt-4'}
      />
      <Title
        text={'Temperature (in degrees Celsius)'}
        size={'S'}
        className={'py-4'}
      />
      <List
        rows={tempData}
        selectors={selectors}
        header={headers}
        cellClassNames={cn}
      />
      <Title
        text={'Amount of precipitation fallen in the last 24 hours (in mm)'}
        size={'S'}
        className={'py-4'}
      />
      <List
        rows={precipitationData}
        selectors={selectors}
        header={headers}
        cellClassNames={cn}
      />
      <Title
        text={'Other'}
        size={'S'}
        className={'py-4'}
      />
      <Paragraph text={`Wind speed: ${weather.windSpeedKmh} km/h`} />
      <Paragraph text={`Cloud coverage: ${weather.cloudsPct} %`} />
      <Paragraph text={`Visibility: ${weather.visibilityKm} km`} />
    </>
  )
}
