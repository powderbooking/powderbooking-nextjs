export interface ResortPageProps {
  params: {
    resortId: number
  }
}
