import { Spinner } from '@/components/spinner/Spinner'
import './globals.css'
import { PropsWithChildren, Suspense } from 'react'
import { Providers } from './Providers'
import { Header } from '@/components/header/Header'

// TODO: proper fallback with skeleton
export default function RootLayout({ children }: PropsWithChildren) {
  return (
    <Providers>
      {/*
        <head /> will contain the components returned by the nearest parent
        head.tsx. Find out more at https://beta.nextjs.org/docs/api-reference/file-conventions/head
      */}
      <head />
      <body className="bg-slate-50 dark:bg-slate-950">
        <div className="flex flex-row">
          <div className="md:flex-1"></div>
          <div className="basis-full max-w-3xl">
            <Header />
            <Suspense fallback={<Spinner />}>{children}</Suspense>
          </div>
          <div className="md:flex-1"></div>
        </div>
      </body>
    </Providers>
  )
}
