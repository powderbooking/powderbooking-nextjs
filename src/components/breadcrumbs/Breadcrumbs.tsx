import classNames from 'classnames'
import Link from 'next/link'

export interface BreadcrumbsProps {
  crumbs: BreadcrumbProps[]
  testid?: string
  className?: string
}

export interface BreadcrumbProps {
  text: string
  href?: string
  leaf?: boolean
  testid?: string
  className?: string
}

const Separator = () => (
  <li>
    <span className="mx-2 text-neutral-500 dark:text-neutral-400">/</span>
  </li>
)

export const Breadcrumb = ({ href, text, leaf = false, testid, className }: BreadcrumbProps) => {
  const cn = classNames(
    {
      'text-primary transition duration-150 ease-in-out hover:text-primary-600 focus:text-primary-600 active:text-primary-700 dark:text-primary-400 dark:hover:text-primary-500 dark:focus:text-primary-500 dark:active:text-primary-600':
        !leaf,
      'text-neutral-500 dark:text-neutral-400': leaf,
    },
    className,
  )
  return (
    <>
      <li>
        {href ? (
          <Link
            data-testid={testid}
            href={href}
            className={cn}>
            {text}
          </Link>
        ) : (
          <div
            data-testid={testid}
            className={cn}>
            {text}
          </div>
        )}
      </li>
      {!leaf && <Separator />}
    </>
  )
}

export const Breadcrumbs = ({ crumbs, className, testid }: BreadcrumbsProps) => {
  return (
    <nav
      data-testid={testid}
      className={classNames('w-full rounded-md', className)}>
      <ol className="list-reset flex">
        {crumbs.map((crumb, index) => (
          <Breadcrumb
            key={crumb.text}
            leaf={index === crumbs.length - 1}
            {...crumb}
          />
        ))}
      </ol>
    </nav>
  )
}
