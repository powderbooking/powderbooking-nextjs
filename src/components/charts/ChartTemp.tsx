'use client'

import React, { useEffect, useRef, useState } from 'react'
import type { ChartData, ChartArea, ChartOptions } from 'chart.js'
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Filler,
  Tooltip,
} from 'chart.js'
import { Chart } from 'react-chartjs-2'

ChartJS.register(CategoryScale, LinearScale, LineElement, PointElement, Filler, Tooltip)

export interface ChartTempProps {
  labels: string[]
  tempMax: number[]
  tempMin: number[]
}

const chartOptions: ChartOptions<'line'> = {
  scales: {
    y: {
      title: {
        display: true,
        text: 'in degrees Celsius',
      },
      type: 'linear',
      ticks: {
        maxTicksLimit: 5,
      },
    },
  },
}

function createMaxGradient(ctx: CanvasRenderingContext2D, area: ChartArea) {
  const gradient = ctx.createLinearGradient(0, area.bottom, 0, area.top)
  gradient.addColorStop(0, 'rgba(255,38,26,1)')
  gradient.addColorStop(0.1, 'rgba(255,38,26,0.5)')
  gradient.addColorStop(1, 'rgba(255,38,26,0)')
  return gradient
}

function createMinGradient(ctx: CanvasRenderingContext2D, area: ChartArea) {
  const gradient = ctx.createLinearGradient(0, area.bottom, 0, area.top)
  gradient.addColorStop(0, 'rgba(0,123,255,0)')
  gradient.addColorStop(0.1, 'rgba(0,123,255,0.5)')
  gradient.addColorStop(1, 'rgba(0,123,255,2)')
  return gradient
}

export const ChartTemp = ({ labels, tempMax, tempMin }: ChartTempProps) => {
  const chartRef = useRef<ChartJS<'line'>>(null)
  const [chartData, setChartData] = useState<ChartData<'line'>>({
    datasets: [],
  })

  useEffect(() => {
    const chart = chartRef.current

    if (chart) {
      setChartData({
        labels,
        datasets: [
          {
            label: 'Freezing point',
            data: [...Array(labels.length).fill(0)],
            borderColor: '#eeeeee',
            borderWidth: 2,
            borderDash: [5, 5],
            pointRadius: 0,
          },
          {
            label: 'Maximum temperature (C)',
            data: tempMax,
            fill: 'end',
            backgroundColor: createMaxGradient(chart.ctx, chart.chartArea),
            borderColor: ['#ff261a'],
            borderWidth: 4,
            pointRadius: 0,
            pointHitRadius: 10,
            tension: 0.2,
          },
          {
            label: 'Minimum temperature (C)',
            data: tempMin,
            fill: 'start',
            backgroundColor: createMinGradient(chart.ctx, chart.chartArea),
            borderColor: ['#007BFF'],
            borderWidth: 4,
            pointRadius: 0,
            pointHitRadius: 10,
            tension: 0.2,
          },
        ],
      })
    }
  }, [labels, tempMax, tempMin])
  return (
    <Chart
      ref={chartRef}
      options={chartOptions}
      type={'line'}
      data={chartData}
    />
  )
}
