import { PropsWithChildren, Suspense } from 'react'
import { ResortHeader } from './ResortHeader'
import { ResortPageProps } from './types'
import { Spinner } from '@/components/spinner/Spinner'

type ResortLayoutProps = ResortPageProps & PropsWithChildren

export default function Layout({ params: { resortId }, children }: ResortLayoutProps) {
  // TODO: fallback to skeleton
  // TODO: break up into several smaller suspenses
  return (
    <div className="flex flex-col">
      <Suspense fallback={<Spinner />}>
        <ResortHeader resortId={resortId} />
        <main>{children}</main>
      </Suspense>
    </div>
  )
}
