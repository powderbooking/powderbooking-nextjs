import { Breadcrumbs } from '@/components/breadcrumbs/Breadcrumbs'
import { Pills } from '@/components/pills/Pills'
import { ResortApi } from '@/lib'

export async function ResortHeader({ resortId }: { resortId: number }) {
  const resort = await new ResortApi().resortGet({ resortId })

  return (
    <>
      <Breadcrumbs
        crumbs={[
          {
            text: 'resorts',
            href: '/',
          },
          {
            text: resort.village ?? '?',
          },
        ]}
        className="my-4"
      />
      <Pills
        pills={[
          {
            text: 'resort',
            href: `/resort/${resortId}`,
            id: 'resort',
          },
          {
            text: 'current weather',
            href: `/resort/${resortId}/weather`,
            id: 'weather',
          },
          {
            text: 'forecast',
            href: `/resort/${resortId}/forecast`,
            id: 'forecast',
          },
          {
            text: 'hotels',
            href: `/resort/${resortId}/hotels`,
            id: 'hotels',
          },
        ]}
      />
    </>
  )
}
