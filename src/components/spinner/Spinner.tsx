import classNames from 'classnames'

export interface SpinnerProps {
  size?: 'S' | 'M' | 'L'
  className?: string
}

export const Spinner = ({ size = 'M', className }: SpinnerProps) => {
  return (
    <div className="flex items-center justify-center">
      <div
        className={classNames(
          'inline-block animate-spin rounded-full border-4 border-solid border-current border-r-transparent align-[-0.125em] motion-reduce:animate-[spin_1.5s_linear_infinite]',
          {
            'h-4 w-4': size === 'S',
            'h-8 w-8': size === 'M',
            'h-12 w-12': size === 'L',
          },
          className,
        )}
        role="status">
        <span className="!absolute !-m-px !h-px !w-px !overflow-hidden !whitespace-nowrap !border-0 !p-0 ![clip:rect(0,0,0,0)]">
          Loading...
        </span>
      </div>
    </div>
  )
}
