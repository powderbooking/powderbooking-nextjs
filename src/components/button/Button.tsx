import React from 'react'
import classNames from 'classnames'

export interface ButtonProps {
  label: string
  onClick: React.MouseEventHandler<HTMLButtonElement>
  testid?: string
  className?: string
}

// ref: https://tailwind-elements.com/
export function Button({ label, onClick, testid, className }: ButtonProps): JSX.Element {
  return (
    <div className="flex space-x-2 justify-center">
      <button
        type="button"
        onClick={onClick}
        data-testid={testid}
        className={classNames(
          'inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out',
          className,
        )}>
        {label}
      </button>
    </div>
  )
}
