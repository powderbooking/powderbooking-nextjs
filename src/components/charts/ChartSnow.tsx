'use client'

import React, { useEffect, useRef, useState } from 'react'
import type { ChartData, ChartArea, ChartOptions } from 'chart.js'
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Filler,
  Tooltip,
} from 'chart.js'
import { Chart } from 'react-chartjs-2'

ChartJS.register(CategoryScale, LinearScale, LineElement, PointElement, Filler, Tooltip)

export interface ChartSnowProps {
  labels: string[]
  snow: number[]
  rain: number[]
}

const chartOptions: ChartOptions<'line'> = {
  scales: {
    y: {
      title: {
        display: true,
        text: 'in mm',
      },
      type: 'linear',
      ticks: {
        maxTicksLimit: 5,
      },
    },
  },
}

function createSnowGradient(ctx: CanvasRenderingContext2D, area: ChartArea) {
  const gradient = ctx.createLinearGradient(0, area.bottom, 0, area.top)
  gradient.addColorStop(0, 'rgba(238,238,238,0)')
  gradient.addColorStop(0.25, 'rgba(238,238,238,0.5)')
  gradient.addColorStop(1, 'rgba(238,238,238,1)')
  return gradient
}

function createRainGradient(ctx: CanvasRenderingContext2D, area: ChartArea) {
  const gradient = ctx.createLinearGradient(0, area.bottom, 0, area.top)
  gradient.addColorStop(0, 'rgba(0,123,255,0)')
  gradient.addColorStop(0.25, 'rgba(0,123,255,0.33)')
  gradient.addColorStop(1, 'rgba(0,123,255,0.7)')
  return gradient
}

export const ChartSnow = ({ labels, snow, rain }: ChartSnowProps) => {
  const chartRef = useRef<ChartJS<'line'>>(null)
  const [chartData, setChartData] = useState<ChartData<'line'>>({
    datasets: [],
  })

  useEffect(() => {
    const chart = chartRef.current

    if (chart) {
      setChartData({
        labels,
        datasets: [
          {
            label: 'Zero line',
            data: [...Array(labels.length).fill(0)],
            borderColor: '#eeeeee',
            borderWidth: 2,
            borderDash: [5, 5],
            pointRadius: 0,
          },
          {
            label: 'Expected snow fall (in mm)',
            data: snow,
            fill: 'origin',
            backgroundColor: createSnowGradient(chart.ctx, chart.chartArea),
            // backgroundColor: ["#56ef00"],
            borderColor: ['#ffffff'],
            borderWidth: 4,
            pointRadius: 0,
            pointHitRadius: 10,
            tension: 0.2,
          },
          {
            label: 'Expected rain fall (in mm)',
            data: rain,
            fill: 'origin',
            backgroundColor: createRainGradient(chart.ctx, chart.chartArea),
            // backgroundColor: ["#9a2424"],
            borderColor: ['#007BFF'],
            borderWidth: 2,
            pointRadius: 0,
            pointHitRadius: 10,
            tension: 0.2,
          },
        ],
      })
    }
  }, [labels, snow, rain])
  return (
    <Chart
      ref={chartRef}
      options={chartOptions}
      type={'line'}
      data={chartData}
    />
  )
}
