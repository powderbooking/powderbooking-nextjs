import classNames from 'classnames'
import { PropsWithChildren } from 'react'

export interface JumbotronProps extends PropsWithChildren {
  className?: string
}

export const Jumbotron = ({ children, className }: JumbotronProps) => {
  return (
    <div
      className={classNames(
        'rounded-lg bg-neutral-100 p-6 text-neutral-700 shadow-lg dark:bg-neutral-600 dark:text-neutral-200 dark:shadow-black/30',
        className,
      )}>
      {children}
    </div>
  )
}
