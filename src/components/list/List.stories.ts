import type { Meta, StoryObj } from '@storybook/react'

import { BaseList } from './List'

// More on how to set up stories at: https://storybook.js.org/docs/7.0/react/writing-stories/introduction
const meta: Meta<typeof BaseList> = {
  title: 'Example/List',
  component: BaseList,
  tags: ['autodocs'],
}

export default meta
type Story = StoryObj<typeof BaseList>

// More on writing stories with args: https://storybook.js.org/docs/7.0/react/writing-stories/args
export const Default: Story = {
  args: {
    header: ['resort', 'snow', 'rain'],
    selectors: ['name', 'snow', 'rain'],
    rows: [
      {
        id: -1,
        name: 'Briancon',
        snow: 7,
        rain: 20,
        href: '#',
      },
      {
        id: 1,
        name: 'Val Thorens and a very long text',
        snow: 42,
        rain: 100,
      },
    ],
    cellClassNames: ['flex-1 min-w-32', 'flex-none w-32 text-center', 'flex-none w-32 text-center'],
  },
}
