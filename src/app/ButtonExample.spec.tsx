import { expect, test } from 'vitest'
import { render, screen } from '@testing-library/react'

import ButtonExample from './ButtonExample'

test('renders a client component', () => {
  render(
    <ButtonExample
      label={'my-label'}
      testid={'my-button'}
    />,
  )

  const button = screen.getByTestId('my-button')

  expect(button).toBeDefined()
})
