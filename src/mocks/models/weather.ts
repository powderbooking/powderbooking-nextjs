import { WeatherDto, WeatherDtoFromJSON } from '@/lib'

export const mockedWeatherJson: any = {
  id: -1000,
  resort_id: -1000,
  date_request: '2023-02-22T08:50:35.601654',
  dt: '2023-02-22T08:50:35.540594',
  date: '2023-02-22',
  timepoint: 2,
  temperature_c: 0.21,
  wind_speed_kmh: 2.42,
  wind_direction_deg: 210,
  visibility_km: 10000,
  clouds_pct: 90,
  snow_3h_mm: 2,
  rain_3h_mm: 2,
}

export const mockedWeather: WeatherDto = WeatherDtoFromJSON(mockedWeatherJson)
