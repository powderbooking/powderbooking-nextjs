import classNames from 'classnames'
import { createElement } from 'react'

export interface TextProps {
  text?: string | number
  as?: 'p' | 'span'
  size?: 'S' | 'M' | 'L'
  testid?: string
  className?: string
}

export const Text = ({ text, as = 'p', size = 'M', className, testid }: TextProps) => {
  const cn = classNames(
    'font-light leading-relaxed text-neutral-500 dark:text-white',
    {
      'text-normal': size === 'S',
      'text-base': size === 'M',
      'text-xl': size === 'L',
    },
    className,
  )

  return createElement(
    as,
    {
      className: cn,
      'data-testid': testid,
    },
    text,
  )
}
