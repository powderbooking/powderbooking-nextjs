import classNames from 'classnames'
import Link from 'next/link'
import { PropsWithChildren } from 'react'

import { Text } from '../text/Text'
import { Title } from '../title/Title'

export type RowData = {
  id?: string | number
  className?: string
  testid?: string
  href?: string
  [key: string]: string | number | undefined
}

export interface BaseListProps<T extends RowData> {
  rows: T[]
  selectors: (keyof T)[]
  header?: string[]
  cellClassNames?: string[]
  tableClassName?: string
  // defaults to "rowData.id"
  idSelector?: (rowData: T) => string | number | undefined
}

export interface List<T extends RowData> extends BaseListProps<T> {
  // create the href based on the row data
  // href defined in the row has precedence over this functor
  hrefCreator?: (row: T) => string
}

export function List<T extends RowData>(props: List<T>) {
  const { rows, hrefCreator, ...otherProps } = props
  if (!hrefCreator) {
    return (
      <BaseList
        {...otherProps}
        rows={rows}
      />
    )
  }

  return (
    <BaseList
      {...otherProps}
      rows={props.rows.map(r => ({ ...r, href: r.href ?? hrefCreator(r) }))}
    />
  )
}

export function BaseList<T extends RowData>({
  header,
  rows,
  idSelector = a => a.id,
  selectors,
  cellClassNames,
  tableClassName,
}: BaseListProps<T>) {
  if (
    cellClassNames &&
    (cellClassNames.length != selectors.length || cellClassNames.length != header?.length)
  ) {
    throw new Error('cellClassNames should be same length as selectors')
  }

  return (
    <ul
      className={classNames(
        'overflow-hidden rounded-lg border border-gray-200 dark:border-neutral-500',
        tableClassName,
      )}>
      {header && (
        <ListHeader
          header={header}
          cellClassNames={cellClassNames}
        />
      )}
      {rows.map(row => (
        <Row
          key={idSelector(row)}
          row={row}
          cellClassNames={cellClassNames}
          selectors={selectors}
        />
      ))}
    </ul>
  )
}

interface RowProps<T extends RowData>
  extends Pick<BaseListProps<T>, 'selectors' | 'cellClassNames'> {
  row: T
}

function Row<T extends RowData>({ row, selectors, cellClassNames }: RowProps<T>) {
  return (
    <li
      data-testid={row.testid}
      className={classNames(
        'border-b last:border-0 border-gray-200 dark:border-neutral-500 transition duration-300 ease-in-out hover:bg-neutral-100 dark:hover:bg-neutral-600',
        row.className,
      )}>
      <PossibleLink
        href={row.href}
        className="flex flex-row">
        {selectors.map((s, i) => {
          const cn = cellClassNames ? cellClassNames[i] : undefined
          return (
            <Text
              key={String(s)}
              text={row[s]}
              className={classNames('truncate px-6 py-4', cn)}
            />
          )
        })}
      </PossibleLink>
    </li>
  )
}

const PossibleLink = ({
  href,
  children,
  ...otherProps
}: PropsWithChildren & { href?: string; className?: string }) => {
  if (!href) return <div {...otherProps}>{children}</div>
  return (
    <Link
      href={href}
      prefetch={false}
      {...otherProps}>
      {children}
    </Link>
  )
}

const ListHeader = ({
  header,
  cellClassNames,
}: {
  header: string[]
  cellClassNames?: string[]
}) => {
  return (
    <li className="flex flex-row border-b last:border-0 border-b-gray-200 transition duration-300 ease-in-out hover:bg-neutral-100 dark:border-neutral-500 dark:hover:bg-neutral-600">
      {header.map((s, i) => {
        const cn = cellClassNames ? cellClassNames[i] : undefined
        return (
          <Title
            key={s}
            className={classNames('px-6 py-4', cn)}
            text={s}
            size="S"
          />
        )
      })}
    </li>
  )
}
