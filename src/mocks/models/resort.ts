import { ResortDto, ResortDtoFromJSON } from '@/lib'

export const mockedResortJson: any = {
  id: -1,
  continent: 'unknown',
  name: '',
  fullname: 'unknown',
  country: 'unknown',
  village: 'unknown',
  lat: 47.1833333,
  lng: 12.6833333,
  altitude_min_m: 1051,
  altitude_max_m: 1978,
  lifts: 17,
  slopes_total_km: 41,
  slopes_blue_km: 13,
  slopes_red_km: 25,
  slopes_black_km: 3,
}

export const mockedResort: ResortDto = ResortDtoFromJSON(mockedResortJson)
