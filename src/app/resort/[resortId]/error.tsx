'use client'
import { Paragraph } from '@/components/paragraph/Paragraph'

export default function Error() {
  return <Paragraph text={"Looks like we don't have any data to show. Come back later!"} />
}
