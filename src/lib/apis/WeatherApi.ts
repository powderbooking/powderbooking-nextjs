/* tslint:disable */
/* eslint-disable */
/**
 * PowderbookingApi
 * Application to show the best hotels with the weather
 *
 * The version of the OpenAPI document: v1
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import * as runtime from '../runtime'
import type { ProblemDetails, ScrapeDto, WeatherDto, WeatherPostDto } from '../models/index'
import {
  ProblemDetailsFromJSON,
  ProblemDetailsToJSON,
  ScrapeDtoFromJSON,
  ScrapeDtoToJSON,
  WeatherDtoFromJSON,
  WeatherDtoToJSON,
  WeatherPostDtoFromJSON,
  WeatherPostDtoToJSON,
} from '../models/index'

export interface WeatherGetRequest {
  resortId: number
}

export interface WeatherPostRequest {
  weatherPostDto?: WeatherPostDto
}

export interface WeatherPostBulkRequest {
  weatherPostDto?: Array<WeatherPostDto>
}

/**
 *
 */
export class WeatherApi extends runtime.BaseAPI {
  /**
   * Get the latest weather report for the given overview identifier
   */
  async weatherGetRaw(
    requestParameters: WeatherGetRequest,
    initOverrides?: RequestInit | runtime.InitOverrideFunction,
  ): Promise<runtime.ApiResponse<WeatherDto>> {
    if (requestParameters.resortId === null || requestParameters.resortId === undefined) {
      throw new runtime.RequiredError(
        'resortId',
        'Required parameter requestParameters.resortId was null or undefined when calling weatherGet.',
      )
    }

    const queryParameters: any = {}

    const headerParameters: runtime.HTTPHeaders = {}

    const response = await this.request(
      {
        path: `/Weather/{resortId}`.replace(
          `{${'resortId'}}`,
          encodeURIComponent(String(requestParameters.resortId)),
        ),
        method: 'GET',
        headers: headerParameters,
        query: queryParameters,
      },
      initOverrides,
    )

    return new runtime.JSONApiResponse(response, jsonValue => WeatherDtoFromJSON(jsonValue))
  }

  /**
   * Get the latest weather report for the given overview identifier
   */
  async weatherGet(
    requestParameters: WeatherGetRequest,
    initOverrides?: RequestInit | runtime.InitOverrideFunction,
  ): Promise<WeatherDto> {
    const response = await this.weatherGetRaw(requestParameters, initOverrides)
    return await response.value()
  }

  /**
   * Insert a new Weather report
   */
  async weatherPostRaw(
    requestParameters: WeatherPostRequest,
    initOverrides?: RequestInit | runtime.InitOverrideFunction,
  ): Promise<runtime.ApiResponse<void>> {
    const queryParameters: any = {}

    const headerParameters: runtime.HTTPHeaders = {}

    headerParameters['Content-Type'] = 'application/json'

    const response = await this.request(
      {
        path: `/Weather`,
        method: 'POST',
        headers: headerParameters,
        query: queryParameters,
        body: WeatherPostDtoToJSON(requestParameters.weatherPostDto),
      },
      initOverrides,
    )

    return new runtime.VoidApiResponse(response)
  }

  /**
   * Insert a new Weather report
   */
  async weatherPost(
    requestParameters: WeatherPostRequest = {},
    initOverrides?: RequestInit | runtime.InitOverrideFunction,
  ): Promise<void> {
    await this.weatherPostRaw(requestParameters, initOverrides)
  }

  /**
   * Insert an array of new Weather reports
   */
  async weatherPostBulkRaw(
    requestParameters: WeatherPostBulkRequest,
    initOverrides?: RequestInit | runtime.InitOverrideFunction,
  ): Promise<runtime.ApiResponse<void>> {
    const queryParameters: any = {}

    const headerParameters: runtime.HTTPHeaders = {}

    headerParameters['Content-Type'] = 'application/json'

    const response = await this.request(
      {
        path: `/Weather/bulk`,
        method: 'POST',
        headers: headerParameters,
        query: queryParameters,
        body: requestParameters.weatherPostDto.map(WeatherPostDtoToJSON),
      },
      initOverrides,
    )

    return new runtime.VoidApiResponse(response)
  }

  /**
   * Insert an array of new Weather reports
   */
  async weatherPostBulk(
    requestParameters: WeatherPostBulkRequest = {},
    initOverrides?: RequestInit | runtime.InitOverrideFunction,
  ): Promise<void> {
    await this.weatherPostBulkRaw(requestParameters, initOverrides)
  }

  /**
   * Get resorts that need to be scraped for weather
   */
  async weatherScrapeRaw(
    initOverrides?: RequestInit | runtime.InitOverrideFunction,
  ): Promise<runtime.ApiResponse<Array<ScrapeDto>>> {
    const queryParameters: any = {}

    const headerParameters: runtime.HTTPHeaders = {}

    const response = await this.request(
      {
        path: `/Weather/scrape`,
        method: 'GET',
        headers: headerParameters,
        query: queryParameters,
      },
      initOverrides,
    )

    return new runtime.JSONApiResponse(response, jsonValue => jsonValue.map(ScrapeDtoFromJSON))
  }

  /**
   * Get resorts that need to be scraped for weather
   */
  async weatherScrape(
    initOverrides?: RequestInit | runtime.InitOverrideFunction,
  ): Promise<Array<ScrapeDto>> {
    const response = await this.weatherScrapeRaw(initOverrides)
    return await response.value()
  }
}
