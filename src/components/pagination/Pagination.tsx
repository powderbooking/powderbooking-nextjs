import classNames from 'classnames'
import { PaginationPill, PaginationPillProps } from '@/components/pagination/PaginationPill'

export interface PaginationProps {
  pills: PaginationPillProps[]
  testid?: string
  className?: string
}

export const Pagination = ({ pills, className, testid }: PaginationProps) => {
  return (
    <div
      className={classNames('flex', className)}
      data-testid={testid}>
      <nav aria-label="Page navigation">
        <ul className="list-style-none flex">
          {pills.map(a => (
            <PaginationPill
              key={a.type || a.label}
              {...a}
            />
          ))}
        </ul>
      </nav>
    </div>
  )
}
