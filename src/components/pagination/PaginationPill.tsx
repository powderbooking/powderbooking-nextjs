import classNames from 'classnames'

export interface PaginationPillProps {
  href: string
  label?: string
  // TODO: 'first'
  type?: 'previous' | 'active' | 'next'
  testid?: string
  className?: string
}

export const PaginationPill = ({ href, label, type, className, testid }: PaginationPillProps) => {
  const cn = classNames(
    'relative block rounded py-1.5 px-3 text-sm transition-all duration-300',
    {
      'bg-primary-100 text-primary-700 font-medium': type === 'active',
      'bg-transparent text-neutral-600 hover:bg-neutral-100 dark:text-white dark:hover:bg-neutral-700 dark:hover:text-white':
        type !== 'active',
    },
    className,
  )

  // TODO: make a function for this
  switch (type) {
    case 'previous':
      return (
        <li>
          <a
            className={cn}
            href={href}
            data-testid={testid}
            aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
          </a>
        </li>
      )
    case 'next':
      return (
        <li>
          <a
            className={cn}
            href={href}
            data-testid={testid}
            aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
          </a>
        </li>
      )
    case 'active':
      return (
        <li aria-current="page">
          <a
            className={cn}
            href={href}
            data-testid={testid}>
            {label}
            <span className="absolute -m-px h-px w-px overflow-hidden whitespace-nowrap border-0 p-0 [clip:rect(0,0,0,0)]">
              (current)
            </span>
          </a>
        </li>
      )
    default:
      return (
        <li>
          <a
            className={cn}
            href={href}
            data-testid={testid}>
            {label}
          </a>
        </li>
      )
  }
}
