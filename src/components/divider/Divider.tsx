import classNames from 'classnames'

export interface DividerProps {
  blurred?: boolean
  vertical?: boolean
  className?: string
}

export const Divider = ({ blurred = false, vertical = false, className }: DividerProps) => {
  if (!vertical) {
    return (
      <hr
        className={classNames(
          'my-12 h-0.5 border-t-0 dark:opacity-50',
          {
            'bg-neutral-100 opacity-100': !blurred,
            'bg-transparent bg-gradient-to-r from-transparent via-neutral-500 to-transparent opacity-25':
              blurred,
          },
          className,
        )}
      />
    )
  }

  return (
    <div
      className={classNames(
        'h-[250px] min-h-[1em] self-stretch dark:opacity-50',
        {
          'inline-block w-0.5 bg-neutral-100 opacity-100': !blurred,
          'w-px bg-gradient-to-tr from-transparent via-neutral-500 to-transparent opacity-20':
            blurred,
        },
        className,
      )}
    />
  )
}
