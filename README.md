# Powderbooking Nextjs

## Generate api

ref: https://openapi-generator.tech/docs/generators/typescript-fetch

Install:
`npm install @openapitools/openapi-generator-cli -g`

Get the swagger specification of the latest backend API - place it in the root (swagger.json)

Generate:

`npx @openapitools/openapi-generator-cli generate -i swagger.json -g typescript-fetch -o ./src/lib/`
