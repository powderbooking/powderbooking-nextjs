import type { Meta, StoryObj } from '@storybook/react'

import { Spinner } from './Spinner'

// More on how to set up stories at: https://storybook.js.org/docs/7.0/react/writing-stories/introduction
const meta: Meta<typeof Spinner> = {
  title: 'Example/Spinner',
  component: Spinner,
  tags: ['autodocs'],
}

export default meta
type Story = StoryObj<typeof Spinner>

const lorem =
  'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis, libero iste quod quibusdam maxime recusandae odit eveniet, aspernatur culpa provident error molestiae vitae corporis in vero est! Beatae, ipsum voluptatibus.'

// More on writing stories with args: https://storybook.js.org/docs/7.0/react/writing-stories/args
export const Default: Story = {
  args: {},
}

export const Small: Story = {
  args: {
    size: 'S',
  },
}

export const Large: Story = {
  args: {
    size: 'L',
  },
}
