import type { Meta, StoryObj } from '@storybook/react'

import { Breadcrumbs } from './Breadcrumbs'

// More on how to set up stories at: https://storybook.js.org/docs/7.0/react/writing-stories/introduction
const meta: Meta<typeof Breadcrumbs> = {
  title: 'Example/Breadcrumbs',
  component: Breadcrumbs,
  tags: ['autodocs'],
}

export default meta
type Story = StoryObj<typeof Breadcrumbs>

const lorem =
  'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis, libero iste quod quibusdam maxime recusandae odit eveniet, aspernatur culpa provident error molestiae vitae corporis in vero est! Beatae, ipsum voluptatibus.'

// More on writing stories with args: https://storybook.js.org/docs/7.0/react/writing-stories/args
export const Default: Story = {
  args: {
    crumbs: [
      {
        text: 'first',
        href: '#',
      },
      {
        text: 'second',
        href: '#',
      },
      {
        text: 'active',
        href: '#',
      },
    ],
  },
}
