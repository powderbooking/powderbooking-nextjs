import { Paragraph } from '@/components/paragraph/Paragraph'
import { ResortPageProps } from '../types'

export default function Page({ params }: ResortPageProps) {
  return <Paragraph text={'Under construction, stay tuned..'} />
}
