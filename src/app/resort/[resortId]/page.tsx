import { ResortApi } from '@/lib'
import { Title } from '@/components/title/Title'
import { Paragraph } from '@/components/paragraph/Paragraph'
import { ResortPageProps } from './types'

export default async function Page({ params: { resortId } }: ResortPageProps) {
  const resort = await new ResortApi().resortGet({ resortId })

  return (
    <>
      <Title text={resort.village} />
      <Paragraph
        text={`${resort.continent} / ${resort.country}`}
        size={'L'}
      />
      <Paragraph
        text={`Altitude: ${resort.altitudeMinM ?? '?'} - ${resort.altitudeMaxM ?? '?'} m`}
      />
      {resort.slopesTotalKm && (
        <Paragraph
          text={`${resort.slopesTotalKm} km of pistes and ${resort.lifts ?? '?'} lifts in total`}
          className={'pt-4'}
        />
      )}
      {resort.slopesBlueKm && (
        <Paragraph
          text={`Blue: ${resort.slopesBlueKm} km`}
          className={'text-blue-700'}
        />
      )}
      {resort.slopesRedKm && (
        <Paragraph
          text={`Red: ${resort.slopesRedKm} km`}
          className={'text-red-600'}
        />
      )}
      {resort.slopesBlackKm && <Paragraph text={`Black: ${resort.slopesBlackKm} km`} />}
    </>
  )
}
