import type { Meta, StoryObj } from '@storybook/react'

import { Pills } from './Pills'

// More on how to set up stories at: https://storybook.js.org/docs/7.0/react/writing-stories/introduction
const meta: Meta<typeof Pills> = {
  title: 'navigation/Pills',
  component: Pills,
  tags: ['autodocs'],
}

export default meta
type Story = StoryObj<typeof Pills>

export const Default: Story = {
  args: {
    pills: [
      {
        text: 'home',
        id: 'home',
        href: '#home',
      },
      {
        text: 'profile',
        id: 'profile',
        href: '#profile',
      },
    ],
  },
}
