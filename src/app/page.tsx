import { RankApi, OverviewDto } from '@/lib'
import {
  OverviewPageProps,
  OverviewProps,
  isNextOverviewProps,
  isPreviousOverviewProps,
} from './types'
import { OverviewPagination } from './OverviewPagination'
import { List } from '@/components/list/List'

export default async function Home({ searchParams }: OverviewPageProps) {
  const { batch, ...paginationProps } = await getOverviews(searchParams)

  return (
    <main>
      <List<OverviewDto>
        header={['resort', 'snow forecast', 'rain forecast']}
        rows={batch}
        selectors={['village', 'snowTotalMm', 'rainTotalMm']}
        cellClassNames={[
          'flex-1 min-w-32',
          'flex-none w-32 text-center',
          'flex-none w-32 text-center',
        ]}
        idSelector={dto => dto.resortId}
        hrefCreator={o => `/resort/${o.resortId}`}
      />
      <OverviewPagination {...paginationProps} />
    </main>
  )
}

async function getOverviews(props?: OverviewProps) {
  const api = new RankApi()
  if (isNextOverviewProps(props)) {
    return await api.rankGetNext(props)
  } else if (isPreviousOverviewProps(props)) {
    return await api.rankGetPrevious(props)
  } else {
    return api.rankGet()
  }
}
