import type { Meta, StoryObj } from '@storybook/react'

import { Jumbotron } from './Jumbotron'

// More on how to set up stories at: https://storybook.js.org/docs/7.0/react/writing-stories/introduction
const meta: Meta<typeof Jumbotron> = {
  title: 'Example/Jumbotron',
  component: Jumbotron,
  tags: ['autodocs'],
}

export default meta
type Story = StoryObj<typeof Jumbotron>

const lorem =
  'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis, libero iste quod quibusdam maxime recusandae odit eveniet, aspernatur culpa provident error molestiae vitae corporis in vero est! Beatae, ipsum voluptatibus.'

// More on writing stories with args: https://storybook.js.org/docs/7.0/react/writing-stories/args
export const Default: Story = {
  args: {
    children: 'simple text',
  },
}
