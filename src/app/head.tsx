export default function Head() {
  return (
    <>
      <title>Powderbooking</title>
      <meta
        content="width=device-width, initial-scale=1"
        name="viewport"
      />
      <meta
        name="description"
        content="Powderbooking - application to show the best hotel deals with the best powdersnow"
      />
      <link
        rel="icon"
        href="/favicon.ico"
      />
    </>
  )
}
