// Copyright (c) 2021. Michael Kemna.

import React from 'react'
import { Meta, StoryObj } from '@storybook/react'
import { ChartSnow } from '@/components/charts/ChartSnow'

// More on how to set up stories at: https://storybook.js.org/docs/7.0/react/writing-stories/introduction
const meta: Meta<typeof ChartSnow> = {
  title: 'Charts/snow',
  component: ChartSnow,
  tags: ['autodocs'],
}

export default meta
type Story = StoryObj<typeof ChartSnow>

const labels = [
  '01-01-2021',
  '02-01-2021',
  '03-01-2021',
  '04-01-2021',
  '05-01-2021',
  '06-01-2021',
  '07-01-2021',
]

export const Straight: Story = {
  args: {
    snow: [5, 5, 5, 5, 5, 5, 5],
    rain: [1, 1, 1, 1, 1, 1, 1],
    labels,
  },
}

export const Wave: Story = {
  args: {
    snow: [1, 3, 5, 3, 1, 3, 5],
    rain: [1, 1, 1, 1, 1, 1, 1],
    labels,
  },
}

export const Waves: Story = {
  args: {
    snow: [1, 3, 5, 3, 1, 3, 5],
    rain: [1, 0, 1, 0, 1, 0, 1],
    labels,
  },
}

export const SnowOnly: Story = {
  args: {
    snow: [1, 3, 5, 3, 1, 3, 5],
    rain: [0, 0, 0, 0, 0, 0, 0],
    labels,
  },
}

export const RainOnly: Story = {
  args: {
    snow: [0, 0, 0, 0, 0, 0, 0],
    rain: [1, 3, 5, 3, 1, 3, 5],
    labels,
  },
}
