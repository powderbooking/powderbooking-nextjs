'use client'

import { PropsWithChildren, createContext } from 'react'
import useLocalStorage from 'use-local-storage'

export interface ThemeContext {
  darkmode: boolean
  toggle: () => void
}
export const ThemeContext = createContext<ThemeContext>({ darkmode: true, toggle: () => {} })

export const ThemeProvider = ({ children }: PropsWithChildren) => {
  const [darkmode, setDarkmode] = useLocalStorage(
    'darkmode',
    typeof window !== 'undefined' && window?.matchMedia('(prefers-color-scheme: dark)').matches,
  )
  const toggle = () => setDarkmode(!darkmode)

  return (
    <ThemeContext.Provider value={{ darkmode, toggle }}>
      <html
        lang="en"
        className={darkmode ? 'dark' : 'light'}>
        {children}
      </html>
    </ThemeContext.Provider>
  )
}
