import type { Meta, StoryObj } from '@storybook/react'

import { Pagination } from './Pagination'

// More on how to set up stories at: https://storybook.js.org/docs/7.0/react/writing-stories/introduction
const meta: Meta<typeof Pagination> = {
  title: 'Example/Pagination',
  component: Pagination,
  tags: ['autodocs'],
}

export default meta
type Story = StoryObj<typeof Pagination>

// More on writing stories with args: https://storybook.js.org/docs/7.0/react/writing-stories/args
export const Default: Story = {
  args: {
    pills: [
      {
        type: 'previous',
        href: '#',
      },
      {
        label: '1',
        href: '#',
      },
      {
        type: 'active',
        label: '2',
        href: '#',
      },
      {
        label: '3',
        href: '#',
      },
      {
        type: 'next',
        href: '#',
      },
    ],
  },
}
