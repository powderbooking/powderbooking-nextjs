import { RankApi, RankGetNextRequest } from '@/lib'
import { NextRequest, NextResponse } from 'next/server'

const isRankGetNextRequest = (value: object): value is RankGetNextRequest => {
  return (
    'rank' in value && !!Number(value.rank) && 'rankingId' in value && !!Number(value.rankingId)
  )
}

// propagation of the batching mechanism
// if we ever want to use a virtualised list in the client
export const GET = async (request: NextRequest) => {
  const requestParams = {
    rankingId: request.nextUrl.searchParams.get('rankingId'),
    rank: request.nextUrl.searchParams.get('rank'),
  }

  if (!isRankGetNextRequest(requestParams)) {
    return new Response(null, { status: 400 })
  }

  const res = await new RankApi().rankGetNext(requestParams)
  return NextResponse.json(res)
}
