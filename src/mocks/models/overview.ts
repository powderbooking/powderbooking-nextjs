import {
  OverviewDto,
  OverviewDtoFromJSON,
  OverviewDtoRankedBatchDto,
  OverviewDtoRankedBatchDtoFromJSON,
} from '@/lib'

export const mockedOverviewJson: any = [
  {
    id: -1,
    village: 'Kaprun',
    lat: 47.1833333,
    lng: 12.6833333,
    rain_week_mm: 14,
    snow_week_mm: 7,
  },
]

export const mockedOverview: OverviewDto[] = mockedOverviewJson.map(OverviewDtoFromJSON)

export const mockedRankedBatchDtoJson: any = {
  batch: mockedOverviewJson,
  rankingId: -1,
}

export const mockedRankedBatchDto: OverviewDtoRankedBatchDto =
  OverviewDtoRankedBatchDtoFromJSON(mockedRankedBatchDtoJson)
