'use client'

import React, { PropsWithChildren, useMemo, useState } from 'react'
import classNames from 'classnames'
import Link from 'next/link'
import { usePathname } from 'next/navigation'

export interface PillProps {
  id: string
  href: string
  text: string
  testid?: string
  className?: string
}

export interface PillsProps extends PropsWithChildren {
  pills: PillProps[]
  testid?: string
  className?: string
}

export function Pills({ pills, testid, className, children }: PillsProps): JSX.Element {
  const pathname = usePathname()
  const pill = useMemo(() => pills.find(value => value.href === pathname), [pills, pathname])
  const [target, setTarget] = useState(pill?.id)

  return (
    <>
      <ul
        className={classNames('mb-5 flex list-none flex-col flex-wrap md:flex-row', className)}
        id="pills-tab"
        role="tablist"
        data-testid={testid}
        data-te-nav-ref>
        {pills.map(p => (
          <li
            key={p.id}
            role="presentation">
            <Link
              onClick={() => setTarget(p.id)}
              href={p.href}
              className={classNames(
                'my-2 block rounded px-7 pt-4 pb-3.5 text-xs font-medium uppercase leading-tight md:mr-4',
                {
                  'text-primary-700 dark:text-primary-700 bg-primary-100': target === p.id,
                  'text-neutral-500 dark:text-white bg-neutral-100 dark:bg-neutral-700':
                    target !== p.id,
                },
                p.className,
              )}
              id={`pills-${p.id}-tab`}
              role="tab"
              aria-controls={`pills-${p.id}`}
              aria-selected={target === p.id}
              data-testid={p.testid}>
              {p.text}
            </Link>
          </li>
        ))}
      </ul>
      {children}
    </>
  )
}
