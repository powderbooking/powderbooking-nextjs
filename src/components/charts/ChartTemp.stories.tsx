// Copyright (c) 2021. Michael Kemna.

import React from 'react'
import { Meta, StoryObj } from '@storybook/react'
import { ChartTemp } from '@/components/charts/ChartTemp'

// More on how to set up stories at: https://storybook.js.org/docs/7.0/react/writing-stories/introduction
const meta: Meta<typeof ChartTemp> = {
  title: 'Charts/temperature',
  component: ChartTemp,
  tags: ['autodocs'],
}

export default meta
type Story = StoryObj<typeof ChartTemp>

const labels = [
  '01-01-2021',
  '02-01-2021',
  '03-01-2021',
  '04-01-2021',
  '05-01-2021',
  '06-01-2021',
  '07-01-2021',
]

export const Straight: Story = {
  args: {
    tempMax: [5, 5, 5, 5, 5, 5, 5],
    tempMin: [1, 1, 1, 1, 1, 1, 1],
    labels,
  },
}

export const Wave: Story = {
  args: {
    tempMax: [1, 3, 5, 3, 1, 3, 5],
    tempMin: [1, 1, 1, 1, 1, 1, 1],
    labels,
  },
}

export const Waves: Story = {
  args: {
    tempMax: [1, 3, 5, 3, 1, 3, 5],
    tempMin: [1, 0, 1, 0, 1, 0, 1],
    labels,
  },
}

export const TempMaxOnly: Story = {
  args: {
    tempMax: [1, 3, 5, 3, 1, 3, 5],
    tempMin: [0, 0, 0, 0, 0, 0, 0],
    labels,
  },
}

export const TempMinOnly: Story = {
  args: {
    tempMax: [0, 0, 0, 0, 0, 0, 0],
    tempMin: [1, 3, 5, 3, 1, 3, 5],
    labels,
  },
}
