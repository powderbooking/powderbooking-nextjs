/* tslint:disable */
/* eslint-disable */
/**
 * PowderbookingApi
 * Application to show the best hotels with the weather
 *
 * The version of the OpenAPI document: v1
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime'
/**
 *
 * @export
 * @interface ForecastDayDto
 */
export interface ForecastDayDto {
  /**
   * Foreign key to the related resort
   * @type {number}
   * @memberof ForecastDayDto
   */
  resortId: number
  /**
   * The date as given by the scraped api
   * @type {Date}
   * @memberof ForecastDayDto
   */
  date: Date
  /**
   * The predicted maximum temperature in Celsius
   * @type {number}
   * @memberof ForecastDayDto
   */
  temperatureMaxC?: number
  /**
   * The predicted minimum temperature in Celsius
   * @type {number}
   * @memberof ForecastDayDto
   */
  temperatureMinC?: number
  /**
   * The predicted amount of rain during the day in millimetres
   * @type {number}
   * @memberof ForecastDayDto
   */
  rainTotalMm?: number
  /**
   * The predicted amount of snow during the day in millimetres
   * @type {number}
   * @memberof ForecastDayDto
   */
  snowTotalMm?: number
  /**
   * The predicted probability of precipitations during the day in percentage
   * @type {number}
   * @memberof ForecastDayDto
   */
  probPrecipPct?: number
  /**
   * The predicted maximum windspeed in km/h
   * @type {number}
   * @memberof ForecastDayDto
   */
  windSpeedMaxKmh?: number
  /**
   * The predicted maximum windspeed of gusts in km/h
   * @type {number}
   * @memberof ForecastDayDto
   */
  windgstMaxKmh?: number
  /**
   * The identifier of the entity
   * @type {number}
   * @memberof ForecastDayDto
   */
  id: number
  /**
   * Ordinal timepoint of the week, categorized every day (range 0-6)
   * 0 - the forecast is given for the same day
   * 6 - the forecast is given for next week
   * @type {number}
   * @memberof ForecastDayDto
   */
  timepoint: number
  /**
   * The date time on which the entity was created in the database (UTC)
   * @type {Date}
   * @memberof ForecastDayDto
   */
  created: Date
}

/**
 * Check if a given object implements the ForecastDayDto interface.
 */
export function instanceOfForecastDayDto(value: object): boolean {
  let isInstance = true
  isInstance = isInstance && 'resortId' in value
  isInstance = isInstance && 'date' in value
  isInstance = isInstance && 'id' in value
  isInstance = isInstance && 'timepoint' in value
  isInstance = isInstance && 'created' in value

  return isInstance
}

export function ForecastDayDtoFromJSON(json: any): ForecastDayDto {
  return ForecastDayDtoFromJSONTyped(json, false)
}

export function ForecastDayDtoFromJSONTyped(
  json: any,
  ignoreDiscriminator: boolean,
): ForecastDayDto {
  if (json === undefined || json === null) {
    return json
  }
  return {
    resortId: json['resort_id'],
    date: new Date(json['date']),
    temperatureMaxC: !exists(json, 'temperature_max_c') ? undefined : json['temperature_max_c'],
    temperatureMinC: !exists(json, 'temperature_min_c') ? undefined : json['temperature_min_c'],
    rainTotalMm: !exists(json, 'rain_total_mm') ? undefined : json['rain_total_mm'],
    snowTotalMm: !exists(json, 'snow_total_mm') ? undefined : json['snow_total_mm'],
    probPrecipPct: !exists(json, 'prob_precip_pct') ? undefined : json['prob_precip_pct'],
    windSpeedMaxKmh: !exists(json, 'wind_speed_max_kmh') ? undefined : json['wind_speed_max_kmh'],
    windgstMaxKmh: !exists(json, 'windgst_max_kmh') ? undefined : json['windgst_max_kmh'],
    id: json['id'],
    timepoint: json['timepoint'],
    created: new Date(json['created']),
  }
}

export function ForecastDayDtoToJSON(value?: ForecastDayDto | null): any {
  if (value === undefined) {
    return undefined
  }
  if (value === null) {
    return null
  }
  return {
    resort_id: value.resortId,
    date: value.date.toISOString().substring(0, 10),
    temperature_max_c: value.temperatureMaxC,
    temperature_min_c: value.temperatureMinC,
    rain_total_mm: value.rainTotalMm,
    snow_total_mm: value.snowTotalMm,
    prob_precip_pct: value.probPrecipPct,
    wind_speed_max_kmh: value.windSpeedMaxKmh,
    windgst_max_kmh: value.windgstMaxKmh,
    id: value.id,
    timepoint: value.timepoint,
    created: value.created.toISOString(),
  }
}
