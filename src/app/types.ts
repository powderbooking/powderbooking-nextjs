export interface OverviewProps {
  rankingId?: number
  rank?: number
  action?: 'next' | 'previous'
}

export type NextOverviewProps = Required<OverviewProps> & {
  action: 'next'
}
export type PreviousOverviewProps = Required<OverviewProps> & {
  action: 'previous'
}

const isOverviewProps = (obj?: OverviewProps): boolean => !!obj?.rank && !!obj?.rankingId
export const isNextOverviewProps = (obj?: OverviewProps): obj is NextOverviewProps =>
  isOverviewProps(obj) && obj?.action == 'next'
export const isPreviousOverviewProps = (obj?: OverviewProps): obj is PreviousOverviewProps =>
  isOverviewProps(obj) && obj?.action == 'previous'

export interface OverviewPageProps {
  searchParams?: OverviewProps
}
