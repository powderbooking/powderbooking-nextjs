/* tslint:disable */
/* eslint-disable */
/**
 * PowderbookingApi
 * Application to show the best hotels with the weather
 *
 * The version of the OpenAPI document: v1
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime'
/**
 *
 * @export
 * @interface BookingTargetsDto
 */
export interface BookingTargetsDto {
  /**
   * The identifier of the resort
   * @type {number}
   * @memberof BookingTargetsDto
   */
  id: number
  /**
   * The Booking destination ID associated with the resort
   * @type {string}
   * @memberof BookingTargetsDto
   */
  bookingDestId: string
}

/**
 * Check if a given object implements the BookingTargetsDto interface.
 */
export function instanceOfBookingTargetsDto(value: object): boolean {
  let isInstance = true
  isInstance = isInstance && 'id' in value
  isInstance = isInstance && 'bookingDestId' in value

  return isInstance
}

export function BookingTargetsDtoFromJSON(json: any): BookingTargetsDto {
  return BookingTargetsDtoFromJSONTyped(json, false)
}

export function BookingTargetsDtoFromJSONTyped(
  json: any,
  ignoreDiscriminator: boolean,
): BookingTargetsDto {
  if (json === undefined || json === null) {
    return json
  }
  return {
    id: json['id'],
    bookingDestId: json['booking_dest_id'],
  }
}

export function BookingTargetsDtoToJSON(value?: BookingTargetsDto | null): any {
  if (value === undefined) {
    return undefined
  }
  if (value === null) {
    return null
  }
  return {
    id: value.id,
    booking_dest_id: value.bookingDestId,
  }
}
