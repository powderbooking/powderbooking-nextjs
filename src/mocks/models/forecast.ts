import { ForecastDto, ForecastDtoFromJSON } from '@/lib'

export const mockedForecastJson: any = {
  id: -1000,
  resort_id: -1000,
  date: '2023-02-22',
  rain_total_mm: 2,
  snow_total_mm: 1,
  forecast_days: [],
}

export const mockedForecast: ForecastDto = ForecastDtoFromJSON(mockedForecastJson)
