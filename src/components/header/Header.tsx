'use client'

import React, { useContext } from 'react'
import { Title } from '../title/Title'
import { Button } from '../button/Button'
import { ThemeContext } from '@/app/ThemeProvider'

export function Header(): JSX.Element {
  const { darkmode, toggle } = useContext(ThemeContext)
  return (
    <div className="flex flex-row w-full text-justify h-14 my-4">
      <Title
        text={'POWDERBOOKING'}
        className="flex-1"
        size={'L'}
      />
      <Button
        label={darkmode ? 'dark' : 'light'}
        onClick={toggle}
        className="flex-none"
      />
    </div>
  )
}
