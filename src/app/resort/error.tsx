'use client'
import { Paragraph } from '@/components/paragraph/Paragraph'

export default function Error() {
  return <Paragraph text={'Unknown resort. Come back later!'} />
}
