import type { Meta, StoryObj } from '@storybook/react'

import { Text } from './Text'

// More on how to set up stories at: https://storybook.js.org/docs/7.0/react/writing-stories/introduction
const meta: Meta<typeof Text> = {
  title: 'Example/Text',
  component: Text,
  tags: ['autodocs'],
}

export default meta
type Story = StoryObj<typeof Text>

const lorem =
  'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis, libero iste quod quibusdam maxime recusandae odit eveniet, aspernatur culpa provident error molestiae vitae corporis in vero est! Beatae, ipsum voluptatibus.'

// More on writing stories with args: https://storybook.js.org/docs/7.0/react/writing-stories/args
export const Default: Story = {
  args: {
    text: lorem,
  },
}

export const Small: Story = {
  args: {
    text: lorem,
    size: 'S',
  },
}

export const Large: Story = {
  args: {
    text: lorem,
    size: 'L',
  },
}

export const Span: Story = {
  args: {
    as: 'span',
    text: lorem,
  },
}
