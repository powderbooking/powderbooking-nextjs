import classNames from 'classnames'
import { Text, TextProps } from '../text/Text'

export const Paragraph = ({ className, ...otherProps }: TextProps) => {
  return (
    <Text
      {...otherProps}
      className={classNames('mt-0 mb-4', className)}
    />
  )
}
