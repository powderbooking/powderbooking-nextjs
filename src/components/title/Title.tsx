import classNames from 'classnames'
import { createElement } from 'react'

export interface TitleProps {
  text?: string | number
  size?: 'S' | 'M' | 'L'
  testid?: string
  className?: string
}

const SizeToClass = {
  S: 'text-xl',
  M: 'text-3xl',
  L: 'text-5xl',
}

const SizeToElement = {
  S: 'h5',
  M: 'h3',
  L: 'h1',
}

export const Title = ({ text, size = 'M', testid, className }: TitleProps) => {
  const cn = classNames('font-medium leading-tight text-primary', SizeToClass[size], className)
  const as = SizeToElement[size]

  return createElement(
    as,
    {
      className: cn,
      'data-testid': testid,
    },
    text,
  )
}
