'use client'

import { PropsWithChildren } from 'react'
import { ThemeProvider } from './ThemeProvider'

export const Providers = ({ children }: PropsWithChildren) => {
  return <ThemeProvider>{children}</ThemeProvider>
}
